INFO
=====
docker-compose-resolved.yaml is autogenerated by kompose.exe 
```
(docker-compose config > docker-compose-resolved.yaml)
```

Steep
=========
1. cd to this root directory
2. create and init directory
    ```
    # for workspace
    sudo mkdir -p /data/jupyter/work
    # for notebook
    sudo mkdir -p /data/jupyter/work/notebook
    # copy pem jupyter certificate
    sudo cp jupyter.pem /data/jupyter/work/notebook/jupyter.pem
    # chmod 775
    sudo chmod 777 -R /data/jupyter/
    ```
3. creating persistent Volume
    ```
    sudo microk8s.kubectl apply -f pv-all-spark-notebook.yaml
    sudo microk8s.kubectl apply -f pv-all-spark-work.yaml
    ```
4. creating Claim persistent Volume
    ```
    sudo microk8s.kubectl apply -f all-spark-notebook-claim0-persistentvolumeclaim.yaml
    sudo microk8s.kubectl apply -f all-spark-notebook-claim1-persistentvolumeclaim.yaml
    ```
5. creating deployment 
    ```
    sudo microk8s.kubectl apply -f all-spark-notebook-deployment.yaml
    ```
6. creating service
    ```
    sudo microk8s.kubectl apply -f all-spark-notebook-service.yaml
    ```

Error Case Handle
==========

restart build container
----------
```
sudo microk8s.kubectl exec -it [POD_NAME] -c [CONTAINER_NAME] -- /bin/sh -c "kill 1"
```

deleting pod
-----------
```
sudo microk8s.kubectl delete pod [POD-NAME]
```
Install Minio using Krew
============
1. make sure already install krew https://krew.sigs.k8s.io/docs/user-guide/setup/install/
    ```
    export PATH="${PATH}:${HOME}/.krew/bin"
    ```
2. Then run the following command to install the MinIO Operator and Plugin:
    ```
    microk8s kubectl krew update
    microk8s kubectl krew install minio
    ```
3. Get from minio github directly
    ```
    wget https://github.com/minio/operator/releases/download/v4.2.7/kubectl-minio_4.2.7_linux_amd64 -O kubectl-minio
    chmod +x kubectl-minio
    sudo mv kubectl-minio /usr/local/bin/
    ```
4. check version
    ```
    microk8s kubectl minio version
    ```
5. Initialize the MinIO Kubernetes Operator
    ```
    microk8s kubectl minio init --output > minio_init.yaml
    ```
6. Then, apply the generated yaml file to Microk8s
    ```
    microk8s kubectl apply -f minio_init.yaml
    ```

Install Minio Browser using Helm3
===============
1. ```microk8s helm3 repo add minio https://helm.min.io/```
2. ```microk8s helm3 install minio minio/minio --set persistence.size=100Gi```
3. get the access key
    ```
    (microk8s.kubectl get secret minio -o jsonpath="{.data.accesskey}" | base64 --decode)
    ```
    dRHSsIVS53jB0AbsoqXP
5. Get the ACCESS_KEY=$(kubectl get secret minio -o jsonpath="{.data.accesskey}" | base64 --decode) and the SECRET_KEY=$(kubectl get secret minio -o jsonpath="{.data.secretkey}" | base64 --decode)
    ```
    (microk8s.kubectl get secret minio -o jsonpath="{.data.secretkey}" | base64 --decode)
    ```
    ypcB8cG8Jbl3fKZrV0sY21uDgDVgOZMr7bV3YLb1

Create a New Tenant
===============
https://docs.onepanel.ai/docs/deployment/configuration/miniotenants/#onepanel-configuration

1. To create a tenant we must first create a namespace.
    ```
    microk8s kubectl create ns example
    ```
2. Then create a file called minio-tenant.yaml
    ```
    ## Secret to be used as MinIO Root Credentials
    apiVersion: v1
    kind: Secret
    metadata:
    namespace: example # your namespace here
    name: minio-autocert-no-encryption-minio-creds-secret
    type: Opaque
    data:
    ## Access Key for MinIO Tenant, base64 encoded (echo -n 'minio' | base64)
    accesskey: bWluaW8=
    ## Secret Key for MinIO Tenant, base64 encoded (echo -n 'minio123' | base64)
    secretkey: bWluaW8xMjM=
    ---
    ## Secret to be used for MinIO Console
    apiVersion: v1
    kind: Secret
    metadata:
    namespace: example # your namespace here
    name: minio-autocert-no-encryption-console-secret
    type: Opaque
    data:
    ## Passphrase to encrypt jwt payload, base64 encoded (echo -n 'SECRET' | base64)
    CONSOLE_PBKDF_PASSPHRASE: U0VDUkVU
    ## Salt to encrypt jwt payload, base64 encoded (echo -n 'SECRET' | base64)
    CONSOLE_PBKDF_SALT: U0VDUkVU
    ## MinIO User Access Key (used for Console Login), base64 encoded (echo -n 'YOURCONSOLEACCESS' | base64)
    CONSOLE_ACCESS_KEY: WU9VUkNPTlNPTEVBQ0NFU1M=
    ## MinIO User Secret Key (used for Console Login), base64 encoded (echo -n 'YOURCONSOLESECRET' | base64)
    CONSOLE_SECRET_KEY: WU9VUkNPTlNPTEVTRUNSRVQ=
    ---
    ## MinIO Tenant Definition
    apiVersion: minio.min.io/v2
    kind: Tenant
    metadata: 
    namespace: example # your namespace here
    name: minio-autocert-no-encryption
    ## Optionally pass labels to be applied to the statefulset pods
    labels:
        app: minio-autocert-no-encryption-minio
    ## Annotations for MinIO Tenant Pods
    annotations:
        prometheus.io/path: /minio/v2/metrics/cluster
        prometheus.io/port: "9000"
        prometheus.io/scrape: "true"

    ## If a scheduler is specified here, Tenant pods will be dispatched by specified scheduler.
    ## If not specified, the Tenant pods will be dispatched by default scheduler.
    # scheduler:
    #  name: my-custom-scheduler

    spec:
    ## Registry location and Tag to download MinIO Server image
    image: minio/minio:RELEASE.2021-08-17T20-53-08Z
    imagePullPolicy: IfNotPresent

    ## Secret with credentials to be used by MinIO Tenant.
    ## Refers to the secret object created above.
    credsSecret:
        name: minio-autocert-no-encryption-minio-creds-secret

    ## Specification for MinIO Pool(s) in this Tenant.
    pools:
        - servers: 1
        volumesPerServer: 4
        volumeClaimTemplate:
            metadata:
            name: data
            spec:
            accessModes:
                - ReadWriteOnce
            resources:
                requests:
                storage: 10Gi # your storage here
    ## Mount path where PV will be mounted inside container(s).
    mountPath: /data
        ## Sub path inside Mount path where MinIO stores data.
        # subPath: /data

    ## Enable automatic Kubernetes based certificate generation and signing as explained in
    ## https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster
    requestAutoCert: false

    ## This field is used only when "requestAutoCert" is set to true. Use this field to set CommonName
    ## for the auto-generated certificate. Internal DNS name for the pod will be used if CommonName is
    ## not provided. DNS name format is *.minio.default.svc.cluster.local
    certConfig:
        commonName: ""
        organizationName: []
        dnsNames: []

    ## PodManagement policy for MinIO Tenant Pods. Can be "OrderedReady" or "Parallel"
    ## Refer https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/#pod-management-policy
    ## for details.
    podManagementPolicy: Parallel

    ## Add environment variables to be set in MinIO container (https://github.com/minio/minio/tree/master/docs/config)
    # env:
    # - name: MINIO_BROWSER
    #   value: "off" # to turn-off browser
    # - name: MINIO_STORAGE_CLASS_STANDARD
    #   value: "EC:2"

    ## PriorityClassName indicates the Pod priority and hence importance of a Pod relative to other Pods.
    ## This is applied to MinIO pods only.
    ## Refer Kubernetes documentation for details https://kubernetes.io/docs/concepts/configuration/pod-priority-preemption/#priorityclass/
    # priorityClassName: high-priority
    ```

    ```
    microk8s kubectl apply -f minio-tenant.yaml
    ```

API & Other DOcumentation Minio
===================
1. for login into minio console root
```
https://10.152.183.158:9090/login
token jwt 

eyJhbGciOiJSUzI1NiIsImtpZCI6IldIQkhabkRVLUtxSWsyNjBBVkJXV0YxS3p6VFZEU25PNm1fR00ySVJzQUUifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJtaW5pby1vcGVyYXRvciIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjb25zb2xlLXNhLXRva2VuLXBqdHFqIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNvbnNvbGUtc2EiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJiMGRjY2Q4NS03M2IwLTRiMzgtOTc5Zi05N2UzODQwZWRkMWEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6bWluaW8tb3BlcmF0b3I6Y29uc29sZS1zYSJ9.giqT20kIo6n4Jv1ID4ZrkcKrcy739Rm2genUjTiuSs9KksBqFDFkSMYhI-Ojiam2DTJpGbcIno5FwlqAzqUWxsr5jCVfXkd2wwMAE9mCQtqGzdxnmVVSmfThpAW12n2XddDrZtVZyIPCSYpeQsctN2dteJ9jiUDLwLmrpNs9pKM814QRMJKntZPlfsQ0ivrWzXH_xEXFRRsvySpLiiGujAZRicKN7G6fGKwOoB74nz6v78K2GlVVpoXCatCPzLUBBD978muTm2RZAwA3kqhBGcc8_MMBsorxBtzi5otXzx_XDSfZO2-3HVLn2iolCc7IhWshpPjpjWck3cTfavVO_g
====================================

```

2. For login under tenant only
use username and password you can access this url 
```
http://10.152.183.140:9090 username = minio password = minio123 
```

3. For accessing Api 

    ```
    http://10.152.183.228:8801 hostname internal
    http://172.197.111.70:8801 hostname external

    "access_key":"5MJ1XF2YDR71S2GMK7KJ","secret_key":"XyYnDqnfpcwKvbKfPsNlrPioTVohHeZOwGCJU7yg"
    ==================
    follor this doc if you use sdk from javascript https://docs.min.io/docs/javascript-client-api-reference
    ```

4. For TEST API
external ip on windows
```
javac -cp ./minio-8.3.3-all.jar /d/research/kubernetes/microk8s/deployment_file/plugins/minio/test/FileUploader.java

java -cp ./minio-8.3.3-all.jar:. FileUploader

```




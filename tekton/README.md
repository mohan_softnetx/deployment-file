Requiretment
============
- kubernetes cluster
- tekton pipeline
    ```
    https://github.com/tektoncd/pipeline/blob/main/docs/install.md#installing-tekton-pipelines
    ```
- tekton triggers
    ```
    microk8s kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
    microk8s kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml
    ```


Intallation
================

Copy ssh to mounted dir
=======
    ```
    cp -R .ssh /data/auth/
    kubectl apply -f pvc_ssh.yaml
    kubectl apply -f pv_ssh.yaml
    ```

1. Create a Task to clone the Git repository
    ```
    microk8s kubectl apply -f 01-git-clone.yaml
    ```
2. Create a Task to build an image and push it to a container registry
    ```
    microk8s kubectl apply -f 02-build-and-push-image.yaml
    ```
3. Create a Task to deploy an image to a Kubernetes cluster
    ```
    microk8s kubectl apply -f 03-deploy-using-kubectl.yaml
    ```
4. Create a pipeline
    ```
    microk8s kubectl apply -f 04-build-and-deploy-pipeline.yaml
    ```
5. Get THe token from docker registry patch in secret-docker-hub.yaml
    ```
    23996e3c-5148-424d-a972-768f909c3749 (from hub.docker.com/viyancs)
    ```
6. Apply the secret to kubernetes
    ```
    microk8s kubectl apply -f secret-docker-hub.yaml
    ```

    alternatively from textplain generator

    ```
    microk8s kubectl create secret docker-registry regcred --docker-username=mohzen --docker-password=Soft710@mel --docker-email=mohank@softnetx.com -n tekton-pipelines
    ```

7. create Secret Account and RBAC permission use secret registry from step 7
    ```
    microk8s kubectl apply -f 05-pipeline-account.yaml
    ```
8. create Persistance volume
    ```
    microk8s kubectl apply -f 06-pipeline-pv.yaml
    ```
9. create Persistance volume claim
    ```
    microk8s kubectl apply -f 07-pipeline-pv-claim.yaml
    ```
10. Prepare the repository that you want to deploy make sure in the root directory have Dockerfile and folder k8s
11. Copy .ssh to /data/app
    ```
    mkdir -p /data/app/
    cp .ssh /data/app/
    ```
12. Create a PipelineRun
    ```
    microk8s kubectl create -f 08-pipeline-run.yaml
    ```
13. All of this configuration only run once next add another project just create pipeline and pipeline run

14. Automate CD CD using tekton triggers
    ```
    microk8s kubectl apply -f trigger-binding.yaml
    ```


Debug & Trace
------------
* find out pipelinerun log
    ```
    tkn pipelinerun describe ecsm-ui-qcpvj(pipeline run name) -n tekton-pipelines
    ```

Deploy New app
====
1. copy ssh public key into repository target
2. create pipeline run like 08 change the value and aplly into server

Monitoring tekton pods isntallations
----
    ```
    microk8s kubectl get pods --namespace tekton-pipelines --watch
    ```
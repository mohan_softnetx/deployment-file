Install grafana
=====
1. cd to this root directory
2. create and init directory
    ```
    # for library
    sudo mkdir -p /data/monitoring/grafana/lib
    # for config
    sudo mkdir -p /data/monitoring/grafana/conf
    # for provisioning
    sudo mkdir -p /data/monitoring/grafana/provisioning
    sudo chmod -R 775 /data/monitoring/grafana/
    ```
3. creating persistent Volume
    ```
    sudo microk8s.kubectl apply -f pv-grafana.yaml
    ```
4. creating Claim persistent Volume
    ```
    sudo microk8s.kubectl apply -f grafana-pv-claim.yaml
    ```
5. creating deployment 
    ```
    sudo microk8s.kubectl apply -f grafana-deployment.yaml
    ```
6. creating service
    ```
    sudo microk8s.kubectl apply -f grafana-service.yaml
    ```

Change config/data/monitoring/grafana/conf/grafana.ini

====
change this config and restart container
```
vim /data/monitoring/grafana/conf/grafana.ini
```

Install prometheus
=====
1. cd to this root directory
2. create and init directory
    ```
    # for data
    sudo mkdir -p /data/monitoring/prometheus/data
    # for config
    sudo mkdir -p /data/monitoring/prometheus/conf
    sudo chmod -R 775 /data/monitoring/prometheus/
    ```
3. creating persistent Volume
    ```
    sudo microk8s.kubectl apply -f pv-prometheus.yaml
    ```
4. creating Claim persistent Volume
    ```
    sudo microk8s.kubectl apply -f prometheus-pv-claim.yaml
    ```
5. creating deployment 
    ```
    sudo microk8s.kubectl apply -f prometheus-deployment.yaml
    ```
6. creating service
    ```
    sudo microk8s.kubectl apply -f prometheus-service.yaml
    ```

edit config
====
```
sudo vim /data/monitoring/prometheus/conf/prometheus.yml
```


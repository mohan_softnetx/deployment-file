-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: softnetx
-- ------------------------------------------------------
-- Server version       8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `softnetx`
--

CREATE DATABASE IF NOT EXISTS `softnetx` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `softnetx`;

--
-- Table structure for table `cloud_credential`
--

DROP TABLE IF EXISTS `cloud_credential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `cloud_credential` (
  `id` int NOT NULL,
  `additional_field1` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `cloud_platform` varchar(255) DEFAULT NULL,
  `cloud_uri` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cloud_credential`
--

LOCK TABLES `cloud_credential` WRITE;
/*!40000 ALTER TABLE `cloud_credential` DISABLE KEYS */;
INSERT INTO `cloud_credential` VALUES (1,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(3,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(5,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(7,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(9,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(12,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(15,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(17,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','admin',NULL),(18,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(19,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(21,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(22,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(23,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(26,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(31,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(35,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(36,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(39,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(43,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(44,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(45,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(48,NULL,'AKIAZJJ73FSKXLGNZBHP','AWS','https://638473415829.signin.aws.amazon.com/console','gS+rqSHL+FzfiTGHacfEn789hu6H3JyfCoLZ9zYa','ash.sethi@softnetx.com',NULL),(49,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(51,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(52,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(53,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(54,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(56,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(66,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(69,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','Aroy',NULL),(71,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com ','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','testuser',NULL),(72,NULL,'AKIAZJJ73FSKXLGNZBHP','AWS','https://638473415829.signin.aws.amazon.com/console','gS+rqSHL+FzfiTGHacfEn789hu6H3JyfCoLZ9zYa','testuser',NULL),(73,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','testuser',NULL),(74,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','mohzen999',NULL),(76,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','mohan_softnetx',NULL),(77,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','mohan_softnetx',NULL),(79,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','jayanthnag',NULL),(80,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','jayanthnag',NULL),(82,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(84,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com ','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','mohank',NULL),(85,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','mohank',NULL),(89,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(90,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(94,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(95,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(96,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(97,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','k46z/s8bz]q0GoSq.]h@APcLKcY/7AmS','pramodpaul',NULL),(98,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','pramodpaul',NULL),(99,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','pramodpaul',NULL),(100,NULL,'AKIAZJJ73FSKVKRAXVNP','AWS','https://638473415829.signin.aws.amazon.com/console','O6YvYKT2f+DQgL/d9bHwum3qKckY0i7oYqVd16Bc','pramodpaul',NULL),(102,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(107,NULL,'AKIAZJJ73FSKRYJ4JYCR','AWS','https://638473415829.signin.aws.amazon.com/console','vpGcFP86y9Tcmuh0XBQhfLTrHjDcIcTw6tCvAWOx','mohzen99991',NULL),(108,NULL,'AKIAZJJ73FSKRYJ4JYCR','AWS','https://638473415829.signin.aws.amazon.com/console','vpGcFP86y9Tcmuh0XBQhfLTrHjDcIcTw6tCvAWOx','mohzen99991',NULL),(109,NULL,'AKIAZJJ73FSKRYJ4JYCR','AWS','https://638473415829.signin.aws.amazon.com/console','vpGcFP86y9Tcmuh0XBQhfLTrHjDcIcTw6tCvAWOx','mohzen99991',NULL),(110,NULL,'AKIAZJJ73FSKRYJ4JYCR','AWS','https://638473415829.signin.aws.amazon.com/console','vpGcFP86y9Tcmuh0XBQhfLTrHjDcIcTw6tCvAWOx','joginder',NULL),(113,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1',NULL),(124,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-09-29 01:22:25'),(145,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-09-29 12:50:49'),(146,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-09-29 13:53:31'),(149,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-02 17:27:08'),(156,NULL,'AKIAZJJ73FSK7PWWG2LS','AWS','','2TDg0ZY5sApTDgRrfKBplg2JAXTye3K7AGwWvD4S','mohantest','2021-10-03 10:13:59'),(158,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','As57Q~g2HNOkLuIGnzLjqvo~RzoT-bDtALCyh','admin','2021-10-03 10:30:34'),(159,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-03 18:59:07'),(160,NULL,'AKIAZJJ73FSK7PWWG2LS','AWS','','2TDg0ZY5sApTDgRrfKBplg2JAXTye3K7AGwWvD4S','admin','2021-10-04 02:24:57'),(162,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-05 00:15:35'),(164,NULL,'AKIAZJJ73FSK7PWWG2LS','AWS','','2TDg0ZY5sApTDgRrfKBplg2JAXTye3K7AGwWvD4S','admin','2021-10-05 16:02:16'),(168,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-10 01:17:55'),(169,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-10 01:39:09'),(170,NULL,'AKIAZJJ73FSK7PWWG2LS','AWS','','2TDg0ZY5sApTDgRrfKBplg2JAXTye3K7AGwWvD4S','admin','2021-10-10 05:38:44'),(171,NULL,'AKIAZJJ73FSK7PWWG2LS','AWS','','2TDg0ZY5sApTDgRrfKBplg2JAXTye3K7AGwWvD4S','admin','2021-10-10 05:53:56'),(172,NULL,'AKIAZJJ73FSK7PWWG2LS','AWS','','2TDg0ZY5sApTDgRrfKBplg2JAXTye3K7AGwWvD4S','admin','2021-10-10 05:55:43'),(173,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-11 01:12:59'),(174,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-13 05:20:33'),(175,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-13 06:42:01'),(176,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-13 11:52:27'),(177,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 01:44:01'),(178,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 02:05:53'),(179,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 02:51:13'),(180,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 03:42:37'),(181,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 04:22:57'),(182,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 09:15:47'),(183,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-22 09:48:14'),(184,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 00:44:41'),(185,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 01:49:24'),(186,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 07:33:45'),(187,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 07:35:49'),(188,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 07:36:39'),(189,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 08:03:47'),(190,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 10:19:36'),(191,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-23 10:51:41'),(192,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-29 14:35:14'),(193,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 10:52:16'),(194,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 12:26:25'),(195,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 19:22:32'),(196,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 19:44:43'),(197,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 22:34:31'),(198,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 22:47:14'),(199,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-10-31 22:54:08'),(200,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-01 00:02:03'),(201,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-01 00:11:06'),(202,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-01 00:35:48'),(203,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-01 00:42:20'),(204,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-01 00:53:57'),(205,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-01 02:49:46'),(206,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-02 07:39:49'),(207,'','93164b59-60a7-44f0-8289-47e67ef7b561','Azure','https://management.azure.com','l.C7Q~XWDHQYFVzVcJhWDTC5EGMFD~JdJ1MdX','admin','2021-11-02 09:05:08'),(208,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-02 10:46:09'),(209,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-03 05:26:42'),(210,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-06 00:19:37'),(211,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-06 00:40:47'),(212,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-06 00:50:01'),(213,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-07 07:57:47'),(214,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-07 08:20:49'),(215,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-07 08:35:02'),(216,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-07 08:52:39'),(217,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-07 09:02:43'),(218,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-08 12:15:15'),(219,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-08 19:41:30'),(220,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-09 11:02:40'),(221,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-09 12:58:19'),(222,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-13 08:23:31'),(223,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-13 08:24:35'),(224,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-11-13 08:30:41'),(225,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-04 01:20:33'),(226,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-05 08:08:36'),(227,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-07 08:44:36'),(228,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-07 09:03:43'),(229,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-07 09:29:10'),(230,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-07 09:41:26'),(231,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 07:10:32'),(232,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 07:17:08'),(233,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 07:36:07'),(234,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 07:48:33'),(235,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 08:43:40'),(236,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 08:53:07'),(237,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 09:13:56'),(238,'AdditionalField1','sagar.thakkar@amkf.com','AWS','aws.com','Secret','1','2021-12-10 17:14:11');
/*!40000 ALTER TABLE `cloud_credential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (239),(239),(239),(1),(1),(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipsec_connection`
--

DROP TABLE IF EXISTS `ipsec_connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `ipsec_connection` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `source` varchar(30) NOT NULL,
  `destination` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipsec_connection`
--

LOCK TABLES `ipsec_connection` WRITE;
/*!40000 ALTER TABLE `ipsec_connection` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipsec_connection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `job_id` int NOT NULL AUTO_INCREMENT,
  `job_name` varchar(100) NOT NULL,
  `job_description` varchar(500) DEFAULT NULL,
  `job_type` varchar(100) DEFAULT NULL,
  `job_sub_type` varchar(100) DEFAULT NULL,
  `server_profile` int DEFAULT NULL,
  `storage_profile` int DEFAULT NULL,
  `job_created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `job_started_timestamp` timestamp NULL DEFAULT NULL,
  `job_completion_timestamp` timestamp NULL DEFAULT NULL,
  `job_status` varchar(100) DEFAULT NULL,
  `job_topic_prefix` varchar(100) DEFAULT NULL,
  `job_enabled` tinyint unsigned DEFAULT '0',
  `flag` int DEFAULT '0',
  `restore_server_profile` int DEFAULT NULL,
  `signaturekey` varchar(500) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `is_minio_job` tinyint DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  UNIQUE KEY `job_name` (`job_name`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (358,'asdfdsf','sdfdfdsf','FULL_DB','DB',251,47,'2021-12-05 08:53:24',NULL,NULL,'JOB_CREATED','DB_MySQL_AZURE_',0,0,250,'asdsdsadasdaaaaa','1',NULL),(359,'asdsdsa','mino','FULL_DB','DB',250,47,'2021-12-05 08:55:24',NULL,NULL,'JOB_CREATED','DB_MySQL_AZURE_',0,0,250,'sfdfsdfsdfsdfdfs','1',NULL),(360,'asdsa','sdasdsad','FULL_DB','DB',251,47,'2021-12-06 06:11:51',NULL,NULL,'RESTORE_DONE','DB_MySQL_AZURE_',0,1,250,'asdfsadfssadfads','1',NULL),(361,'asdfsdf','asdf','FULL_DB','DB',251,47,'2021-12-06 13:09:29',NULL,NULL,'JOB_CREATED','DB_MySQL_AZURE_',0,1,250,'sadadadasdasdada','1',NULL),(362,'test replication 1','test replication 1',NULL,'REPLICATION',250,NULL,'2021-12-08 10:36:35',NULL,NULL,'JOB_CREATED',NULL,0,0,250,'wefwa25dsf252352','1',1),(363,'asdf','adsfdf','FULL_DB','DB',251,47,'2021-12-08 12:40:42',NULL,NULL,'BACK_UP_COMPLETED','DB_MySQL_AZURE_',0,1,250,'1212121212121212','1',NULL),(364,'asree','safdf','FULL_DB','DB',251,130,'2021-12-08 12:47:32',NULL,NULL,'BACK_UP_STARTED','DB_MySQL_AZURE_',0,1,250,'asdfdfsdfdfsd213','1',NULL),(365,'sdfdf','asfddf',NULL,'REPLICATION',250,NULL,'2021-12-08 12:50:24',NULL,NULL,'JOB_CREATED',NULL,0,0,250,'sfdfsdfdfsdfdfsf','1',1),(366,'asdsdasd','ADSSDAS','FULL_DB','DB',252,47,'2021-12-10 12:22:45',NULL,NULL,'BACK_UP_COMPLETED','DB_MySQL_AZURE_',0,1,250,'asdasdasdasdasda','1',NULL),(367,'rererwere','rerer',NULL,'REPLICATION',252,NULL,'2021-12-10 12:27:44',NULL,NULL,'JOB_CREATED',NULL,0,0,252,'asdasdasdasdasas','1',1),(368,'rewrerewr','wererewr',NULL,'REPLICATION',251,NULL,'2021-12-10 12:29:20',NULL,NULL,'JOB_CREATED',NULL,0,0,250,'ADASDASDASDASDAS','1',1);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_schedule`
--

DROP TABLE IF EXISTS `job_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `job_schedule` (
  `schedule_id` int NOT NULL AUTO_INCREMENT,
  `job_name` varchar(100) NOT NULL,
  `start_time` varchar(100) DEFAULT NULL,
  `duration` int DEFAULT NULL,
  `period` varchar(100) DEFAULT NULL,
  `day_mon` tinyint(1) DEFAULT NULL,
  `day_tue` tinyint(1) DEFAULT NULL,
  `day_wed` tinyint(1) DEFAULT NULL,
  `day_thu` tinyint(1) DEFAULT NULL,
  `day_fri` tinyint(1) DEFAULT NULL,
  `day_sat` tinyint(1) DEFAULT NULL,
  `day_sun` tinyint(1) DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`schedule_id`),
  UNIQUE KEY `job_name` (`job_name`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_schedule`
--

LOCK TABLES `job_schedule` WRITE;
/*!40000 ALTER TABLE `job_schedule` DISABLE KEYS */;
INSERT INTO `job_schedule` VALUES (314,'asdfdsf',NULL,0,NULL,0,0,0,0,0,0,0,0),(315,'asdsdsa',NULL,0,NULL,0,0,0,0,0,0,0,0),(316,'asdsa',NULL,0,NULL,0,0,0,0,0,0,0,0),(317,'asdfsdf',NULL,0,NULL,0,0,0,0,0,0,0,0),(318,'test replication 1',NULL,0,NULL,0,0,0,0,0,0,0,0),(319,'asdf',NULL,0,NULL,0,0,0,0,0,0,0,0),(320,'asree',NULL,0,NULL,0,0,0,0,0,0,0,0),(321,'sdfdf',NULL,0,NULL,0,0,0,0,0,0,0,0),(322,'asdsdasd',NULL,0,NULL,0,0,0,0,0,0,0,0),(323,'rererwere',NULL,0,NULL,0,0,0,0,0,0,0,0),(324,'rewrerewr',NULL,0,NULL,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `job_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server_profile`
--

DROP TABLE IF EXISTS `server_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `server_profile` (
  `profile_id` int NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(100) DEFAULT NULL,
  `server_type` varchar(100) DEFAULT NULL,
  `db_server_host` varchar(200) DEFAULT NULL,
  `db_server_port` int DEFAULT NULL,
  `db_database_name` varchar(100) DEFAULT NULL,
  `db_username` varchar(100) DEFAULT NULL,
  `db_password` varchar(100) DEFAULT NULL,
  `db_server_time_zone` varchar(100) DEFAULT NULL,
  `flag` int DEFAULT '0',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `profile_name` (`profile_name`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server_profile`
--

LOCK TABLES `server_profile` WRITE;
/*!40000 ALTER TABLE `server_profile` DISABLE KEYS */;
INSERT INTO `server_profile` VALUES (250,'azreplication','MySQL','localhost',3306,'AzSinkDB','root','Soft710@mel','EST',0),(251,'policysql','MySQL','localhost',3306,'sakilab','root','Soft710@mel','IST',0),(252,'TSTTEST','MySQL','localhost',3306,'sakilab','root','Soft710@mel','IST',0);
/*!40000 ALTER TABLE `server_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `storage_profile`
--

DROP TABLE IF EXISTS `storage_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `storage_profile` (
  `profile_id` int NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(100) DEFAULT NULL,
  `cloud_name` varchar(100) DEFAULT NULL,
  `cloud_container_name` varchar(100) DEFAULT NULL,
  `storage_connection_string` varchar(1000) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `profile_name` (`profile_name`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `storage_profile`
--

LOCK TABLES `storage_profile` WRITE;
/*!40000 ALTER TABLE `storage_profile` DISABLE KEYS */;
INSERT INTO `storage_profile` VALUES (47,'AWS_S3_POLICY','AWS','JOB_BUCKET','',NULL),(129,'uscentralbucket','AZURE','uscentralbucket','DefaultEndpointsProtocol=https;AccountName=uscentralbucket;AccountKey=xvDYS4ClO4h7C99toi5QlkaXtQ8ucuI/Fu3KYrnjn5YIU+xPVoXAVoMVimpgFpyARxg3VtjoMR1d55Ab/vl/kQ==;EndpointSuffix=core.windows.net',NULL),(130,'testbuck','AZURE','eastbuck','DefaultEndpointsProtocol=https;AccountName=eastbuck;AccountKey=oV4n1vqjh7/hMfN+B52nJFKBp6txbuacmc6u1GkpG+EDbi17WFio0YJa1QhY8voRAHrDIaHeQn//3pQGMtHxaw==;EndpointSuffix=core.windows.net',NULL);
/*!40000 ALTER TABLE `storage_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutorials_tbl`
--

DROP TABLE IF EXISTS `tutorials_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `tutorials_tbl` (
  `tutorial_id` int NOT NULL AUTO_INCREMENT,
  `tutorial_title` varchar(100) NOT NULL,
  `tutorial_author` varchar(40) NOT NULL,
  `submission_date` date DEFAULT NULL,
  PRIMARY KEY (`tutorial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutorials_tbl`
--

LOCK TABLES `tutorials_tbl` WRITE;
/*!40000 ALTER TABLE `tutorials_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `tutorials_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int NOT NULL,
  `active` bit(1) NOT NULL,
  `created_date` date DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `flag` int DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (13,_binary '','2020-08-21','mohzen777@gmail.com','admin','admin',NULL,'$2a$12$75sjGDu3E4/SjFkJ0GBAGOsKHNPRZKtcPpWZjm9j48lnSQNKz0rpi','ROLE_AWS,ROLE_AZURE','2020-08-21','admin','ADMIN',0),(151,_binary '','2021-10-02','koji.gitlab@gmail.com','Joko','Santoso',NULL,'$2a$10$8PYBLuNbcb3rIdNBEUUALuj8z5WGlfOk0F1T0NQhzVO2Yzlm.hk7q','ROLE_AWS','2021-10-02','jokos','user',1),(153,_binary '','2021-10-03','sxe2koji@yahoo.com','Joko','Santoso',NULL,'$2a$10$He8qBfu7pdiwngF1ZfU2s.qJPycYunGdF32e5eVGoALhoWTtz.66O','ROLE_AWS','2021-10-03','jsantoso','user',0),(167,_binary '','2021-10-09','mohank@softnetx.com','moh','jam',NULL,'$2a$10$POxpSv2panyxJjBDZ0XW6.cRAzHi2zvKyq01fHei4vkVvvinvG3.6','ROLE_AWS','2021-10-09','mohjam','user',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-11  9:28:50
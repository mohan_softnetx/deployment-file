Install
=======

1. init the requiretment
```
chmod +x init.sh
./init.sh
```

Test
===

Run this manually before integrate with tekton pipeline
```
microk8s kubectl apply -f k8s/deployment.yaml
```

Change the config
====

open ```k8s/deployment.yaml``` and update the configmap mysql 

Tekton integration
===
1. create pipeline run
2. run the pipeline
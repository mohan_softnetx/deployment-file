THis ingress use for open all port
cek port
===
 sudo lsof -i -P -n | grep LISTEN

How to add new open port in Ingress
1. adding/edit on daemonset configuration :
  ```
      ports:
        - name: http
          containerPort: 80
          hostPort: 80
        - name: https
          containerPort: 443
          hostPort: 443
        - name: health
          containerPort: 10254
          hostPort: 10254
        - name: minio-api
          containerPort: 8801 #comign from service port
          hostPort: 8801
        - name: ecsm-ui
          containerPort: 8802 #comign from service port
          hostPort: 8802

  ```
2. adding mapping into  config map nginx-ingress-tcp-microk8s-conf namespace ingress
  ```
  ref::https://github.com/ubuntu/microk8s/issues/1471
  data:
    '8801': example/minio:8801
  ```
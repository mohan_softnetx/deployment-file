Installing Spark
======
Download Spark from the Spark Download page and extract it with tar xfz ~/Downloads/spark-3.1.2-bin-hadoop3.2.tgz Make the spark shell accessible  by edit .bashrc and save this configuration
```
export SPARK_HOME=/home/mohzen/lib/spark-3.2.0-bin-hadoop3.2
```
and 
```
alias spark-shell='/home/mohzen/lib/spark-3.2.0-bin-hadoop3.2/bin/spark-shell'
```

Install Spark master and worker on kubernetes microk8s with

```
sudo microk8s kubectl apply -f https://bitbucket.org/mohan_softnetx/deployment-file/raw/f162c7d9fede61e5a3c12651d68e3b279823e471/k8s-spark-cluster.yaml
```
or 

Install jupyter+spark bundle on kubernetes microk8s with
```
sudo microk8s.kubectl create ns spark
sudo microk8s kubectl apply -n spark -f https://bitbucket.org/mohan_softnetx/deployment-file/raw/f162c7d9fede61e5a3c12651d68e3b279823e471/jupyter.yml
```

#permission issue
----
```
sudo microk8s.kubectl create serviceaccount tekton-pipelines --namespace=default
sudo microk8s.kubectl create clusterrolebinding spark --clusterrole=edit --serviceaccount=tekton-pipelines:default --namespace=default

```

# comand line
=====

```
sudo microk8s.kubectl -n {namespace} get secrets
sudo microk8s.kubectl -n {namespace} describe secret default-token-{random}
wget http://www.java2s.com/Code/JarDownload/helloworld-/helloworld-2.0.jar.zip

export K8S_TOKEN=eyJhbGciOiJSUzI1NiIsImtpZCI6IldIQkhabkRVLUtxSWsyNjBBVkJXV0YxS3p6VFZEU25PNm1fR00ySVJzQUUifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6ImRlZmF1bHQtdG9rZW4ta2hjY2wiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGVmYXVsdCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImE4ZjZhZjczLWIxZGUtNGJmNy05Nzc4LWExMzM4Nzk0YTY4NyIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmRlZmF1bHQifQ.U6VrIWMAxuRQ9R66d_i4-_Fxhw0V5tolczO_2jsZlf5fAdw0ntsfL602Rsm6yRpMBwsMg21odxdzghFZFjUgU78sn66N58LFLNz1dcwscCKTmQXg2yqqzz7DqYci9BQcvWZBQtvvJETKf9rB6uhQHd-jqUHTqDFzrgGRLvE0IkU2eF4lWZw-0VwVp-u6BBWIuHX5Z60gwXflL0gZBNjNcjg_FiRUNBcmxM6DKEGpt6kZz3v7r2KCPNOw3gr98NAmK2AnyZ1tacfaVmDM0UCv7gi6bC1DMbeTP3FX88kdAUwBXHPEdsxe6aDWVJnj6Pn_k4Bv_T4URmDh8hqarRssEg


$SPARK_HOME/bin/spark-submit --master k8s://192.168.0.222:16443 --deploy-mode cluster --conf spark.kubernetes.namespace=default --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark --name spark-pi --class org.apache.spark.examples.SparkPi --conf spark.kubernetes.authenticate.caCertFile=/var/snap/microk8s/current/certs/ca.crt --conf spark.kubernetes.authenticate.submission.oauthToken=$K8S_TOKEN --conf spark.kubernetes.container.image=bde2020/spark-master:3.1.1-hadoop3.2 --conf spark.executor.instances=2 --conf spark.executor.instances=2 local:///app/helloworld-2.0.jar

# client mode (working)
$SPARK_HOME/bin/spark-submit --master spark://10.1.37.66:7077 --deploy-mode client --name spark-pi --class org.apache.spark.examples.SparkPi --conf spark.kubernetes.namespace=default --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark --conf spark.executor.instances=3 --conf spark.driver.memory=20G --conf spark.executor.memory=8G  file://$SPARK_HOME/examples/jars/spark-examples_2.12-3.2.1.jar

# cluster mode (working)
#pv
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-spark-driver
  namespace: default
  labels:
    type: local
spec:
  storageClassName: pv-spark-driver
  capacity:
    storage: 50Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/data/spark/driver"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-spark-cluster-conf
  namespace: default
  labels:
    type: local
spec:
  storageClassName: pv-spark-cluster-conf
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/data/spark/cluster-conf"
#pvc
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  creationTimestamp: null
  namespace: default
  labels:
    io.kompose.service: pvc-spark-driver
  name: pvc-spark-driver
spec:
  storageClassName: pv-spark-driver
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 30Gi
status: {}
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  creationTimestamp: null
  namespace: default
  labels:
    io.kompose.service: pvc-spark-cluster-conf
  name: pvc-spark-cluster-conf
spec:
  storageClassName: pv-spark-cluster-conf
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 2Gi
status: {}


#submit
$SPARK_HOME/bin/spark-submit --master k8s://192.168.0.222:16443 --deploy-mode cluster --name spark-api --class org.apache.spark.examples.SparkPi --conf spark.kubernetes.namespace=default --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark --conf spark.kubernetes.authenticate.caCertFile=/var/snap/microk8s/current/certs/ca.crt  --conf spark.kubernetes.authenticate.submission.oauthToken=$K8S_TOKEN --conf spark.executor.instances=3 --conf spark.driver.memory=1G --conf spark.executor.memory=1G --conf spark.kubernetes.driver.limit.cores=500m --conf spark.kubernetes.executor.limit.cores=500m --conf spark.num.executor=3 --conf spark.kubernetes.container.image.pullSecrets=regcred  --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.pv-spark-driver.options.claimName=pvc-spark-driver --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.pv-spark-driver.mount.path=/data --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.pv-spark-cluster-conf.options.claimName=pvc-spark-cluster-conf --conf spark.kubernetes.driver.volumes.persistentVolumeClaim.pv-spark-cluster-conf.mount.path=/usr/local/spark/conf --conf spark.dynamicAllocation.enabled=true --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.data.options.claimName=OnDemand --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.data.options.storageClass=microk8s-hostpath --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.data.options.sizeLimit=1Gi --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.data.mount.path=/data/spark/cluster --conf spark.kubernetes.executor.volumes.persistentVolumeClaim.data.mount.readOnly=false --conf spark.kubernetes.container.image=index.docker.io/mohzen/spark:latest --conf spark.shuffle.service.enabled=true local:///opt/spark/examples/jars/spark-examples_2.12-3.2.1.jar

# Run on a Spark standalone cluster in cluster deploy mode with supervise (not working)
$SPARK_HOME/bin/spark-submit --master k8s://192.168.0.222:16443 --deploy-mode cluster --name spark-cluster-mode --class org.apache.spark.examples.SparkPi --conf spark.kubernetes.namespace=default --conf spark.kubernetes.authenticate.driver.serviceAccountName=tekton-pipelines --conf spark.executor.instances=3 --conf spark.driver.memory=20G --conf spark.executor.memory=20G --conf spark.num.executor=50 --conf spark.kubernetes.authenticate.caCertFile=/app/ca.crt  --conf spark.kubernetes.authenticate.submission.oauthToken=$K8S_TOKEN --conf spark.kubernetes.container.image=spark-pi file:///spark/examples/jars/spark-examples_2.12-3.1.1.jar


# Fix error case 
--conf spark.executorEnv.JAVA_HOME="/usr/lib/jvm/java-8-openjdk"
kubectl create clusterrolebinding default \
  --clusterrole=edit --serviceaccount=default:default --namespace=default

```
